module.exports = {
  async up(db, client) {
    db.createCollection('reviews', {
      validator: {
        $jsonSchema: {
          bsonType: 'object',
          additionalProperties: true,
          required: ['uuid', 'product_id', 'reviewer_name', 'reviewer_uuid', 'title', 'description'],
          properties: {
            uuid: {
              bsonType: 'string',
              description: 'must be a string and is required'
            },
            product_id: {
              bsonType: 'objectId',
              description: 'must be an objectId relating to a product and is required'
            },
            reviewer_name: {
              bsonType: 'string',
              description: 'must be a string and is required'
            },
            reviewer_uuid: {
              bsonType: 'string',
              description: 'must be a string and is required'
            },
            title: {
              bsonType: 'string',
              description: 'must be a string and is required'
            },
            description: {
              bsonType: 'string',
              description: 'must be a string and is required'
            }
          }
        }
      },
      validationLevel: 'strict',
      validationAction: 'error'
    });
  },

  async down(db, client) {
    db.reviews.drop();
  }
};
