module.exports = {
  async up(db, client) {
    db.createCollection('products', {
      validator: {
        $jsonSchema: {
          bsonType: 'object',
          additionalProperties: true,
          required: ['uuid', 'name', 'brand', 'price', 'ratings', 'dimensions', 'category_id', 'image_id'],
          properties: {
            name: {
              bsonType: 'string',
              description: 'must be a string and is required'
            },
            uuid: {
              bsonType: 'string',
              description: 'must be a string and is required'
            },
            brand: {
              bsonType: 'string',
              description: 'must be a string and is required'
            },
            price: {
              bsonType: 'int',
              description: 'must be an integer and is required'
            },
            ratings: {
              bsonType: 'double',
              description: 'must be a double and set to default 0.0'
            },
            colors: {
              bsonType: ['array'],
              description: 'must be an array of strings if present'
            },
            dimensions: {
              bsonType: 'object',
              required: [ 'weight', 'height', 'length', 'width' ],
              properties: {
                weight: {
                  bsonType: 'int',
                  description: 'must be an integer (grams) and is required'
                },
                length: {
                  bsonType: 'int',
                  description: 'must be an integer (cm) and is required'
                },
                width: {
                  bsonType: 'int',
                  description: 'must be an integer (cm) and is required'
                },
                height: {
                  bsonType: 'int',
                  description: 'must be an integer (cm) and is required'
                },
              }
            },
            category_id: {
              bsonType: 'objectId',
              description: 'must be an objectId relating to a category and is required'
            },
            image_id: {
              bsonType: 'objectId',
              description: 'must be an objectId relating to an image and is required'
            }
          }
        }
      },
      validationLevel: 'strict',
      validationAction: 'error'
    });
  },

  async down (db, client) {
    db.products.drop();
  }
};
