module.exports = {
  async up(db, client) {
    db.createCollection('images', {
      validator: {
        $jsonSchema: {
          bsonType: 'object',
          additionalProperties: false,
          required: ['path'],
          properties: {
            url: {
              bsonType: 'string',
              description: 'must be a string and is required'
            }
          }
        }
      },
      validationLevel: 'strict',
      validationAction: 'error'
    });
  },

  async down(db, client) {
    db.images.drop();
  }
};
