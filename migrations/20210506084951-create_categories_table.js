module.exports = {
  async up(db, client) {
    db.createCollection('categories', {
      validator: {
        $jsonSchema: {
          bsonType: 'object',
          additionalProperties: false,
          required: ['uuid', 'name'],
          properties: {
            name: {
              bsonType: 'string',
              description: 'must be a string and is required'
            },
            uuid: {
              bsonType: 'string',
              description: 'must be a string and is required'
            }
          }
        }
      },
      validationLevel: 'strict',
      validationAction: 'error'
    });
  },

  async down(db, client) {
    db.categories.drop();
  }
};
