var express = require('express');
var router = express.Router();

/* GET index. */
router.get('/', function (req, res, next) {
  res.send('Hello World!');
});

/* GET show. */
router.get('/:uuid', function (req, res, next) {
  res.send('Hello World!');
});

/* POST store. */
router.get('/', function (req, res, next) {
  res.send('Hello World!');
});

/* PUT update. */
router.put('/:uuid', function (req, res, next) {
  res.send('Hello World!');
});

/* DELETE destroy. */
router.delete('/:uuid', function (req, res, next) {
  res.send('Hello World!');
});

module.exports = router;
