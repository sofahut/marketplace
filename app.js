const express = require('express');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const mongoose = require('mongoose');
require('dotenv').config();

const marketplaceRouter = require('./routes/marketplace');

const app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use('/marketplace', marketplaceRouter);

mongoose.connect(process.env.DATABASE_URL + '/' + process.env.DATABASE_NAME, { useNewUrlParser: true, useUnifiedTopology: true });
const db = mongoose.connection;
db.on('error', (err) => {
  console.log(err);
});

module.exports = app;
